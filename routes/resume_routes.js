var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');

router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });
});

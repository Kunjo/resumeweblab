var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'select street, zip_code from address ' +
        'WHERE address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO address (street) VALUES (?)';

    var queryData = [params.street];

    connection.query(query, params.street, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var address = result.street;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO address (street, zip_code) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var AddressData = [];
        if (params.street.constructor === Array) {
            for (var i = 0; i < params.street.length; i++) {
                AddressData.push([address, params.street[i]]);
            }
        }
        else {
            AddressData.push([address, params.street]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [AddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(street, callback) {
    var query = 'DELETE FROM address WHERE street = ?';
    var queryData = [street];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'select skill_name, description from skill ' +
        'WHERE skill_id = ?';
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO skill (skill_name) VALUES (?)';

    var queryData = [params.skill_name];

    connection.query(query, params.skill_name, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var skill = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO skill (skill_name, description) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var skillData = [];
        if (params.skill_name.constructor === Array) {
            for (var i = 0; i < params.skill_name.length; i++) {
                skillData.push([skill, params.skill_name[i]]);
            }
        }
        else {
            skillData.push([skill, params.skill_name]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [skillData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(skill, callback) {
    var query = 'DELETE FROM skill WHERE skill_name = ?';
    var queryData = [skill];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};
